# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result_hash = {}

  str.split.each do |word|
    result_hash[word] = word.length
  end

  result_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted_hash = hash.sort_by {|k, num| num}

  sorted_hash.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)

  newer.keys.each do |stone|
    older[stone] = newer[stone]
  end

  older
end

def letter_counts(word)
  letter_hash = Hash.new(0)

  word.chars.each do |letter|
    letter_hash[letter] += 1
  end

  letter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = {}

  arr.each do |el|
    uniq_hash[el] = 0
  end

  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  result_hash = { :even => 0, :odd => 0 }

  numbers.each do |el|
    if el.even?
      result_hash[:even] += 1
    else
      result_hash[:odd] += 1
    end
  end

  result_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  letter_hash = Hash.new(0)
  vowels = "aeiou"

  string.each_char do |letter|
    letter_hash[letter] += 1 if vowels.include?(letter.downcase)
  end

  highest = letter_hash.values.sort.last

  letter_hash.select {|k, v| v == highest}.keys.last
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  result = []
  winter_students = students.select {|name, mon| mon > 6}.keys

  result = []
  winter_students.each_with_index do |name, idx|
    winter_students[(idx+1)..-1].each_with_index do |name2, idx2|
      pair = []
      pair = [name, name2]
      result << pair
    end
  end

  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimen_hash = Hash.new(0)

  specimens.each do |animal|
    specimen_hash[animal] += 1
  end

  num_species = specimens.uniq.count

  smallest_pop = specimen_hash.sort_by { |anim, count| count }.first.last

  largest_pop = specimen_hash.sort_by { |anim, count| count }.last.last

  num_species**2 * smallest_pop / largest_pop
end


# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal = remove_punctuation(normal_sign)
  normal_hash = character_count(normal)

  vandalized = remove_punctuation(vandalized_sign)
  vandalized_hash = character_count(vandalized)

  vandalized_hash.all? do |letter, count|
    vandalized_hash[letter] <= normal_hash[letter]
  end

end

def character_count(str)
  count_hash = Hash.new(0)

  str.chars.each do |letter|
    count_hash[letter] += 1
  end

  count_hash
end

def remove_punctuation(str)
  str.downcase.chars.reject {|char| " .,;:'!?".include?(char) }.join
end
